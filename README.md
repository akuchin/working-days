# README #

Calculates working days and working duration based on strategies. 

* **DurationCalculator** - main calculator;

_interface WorkingDaysStrategy_, predefined:

* **AllDaysWorkingStrategy** - default strategy when all days are working;
* **WeekendsStrategy** - all saturdays and sundays are non-working;
* **ChainOfStrategies** - to combine several strategies

_interface WorkingHoursStrategy_, predefined:

* **TwentyFourHoursStrategy** - full day is working
* **WorkingHoursWithLunchTimeStrategy** - configurable working hours (by default 9-18 with No LunchTime)

_interface LunchTime_, predefined:

* **DefaultLunchTime** - allows to define lunch time during day
* **NoLunchTime** - no lunch time (default)
