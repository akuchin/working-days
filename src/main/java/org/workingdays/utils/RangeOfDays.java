package org.workingdays.utils;

import java.time.LocalDate;
import java.util.Iterator;

public class RangeOfDays implements Iterable<LocalDate> {

    private final LocalDate from;
    private final LocalDate to;

    private RangeOfDays(LocalDate from, LocalDate to) {
        this.from = from;
        this.to = to;
    }

    public static RangeOfDays of(LocalDate from, LocalDate to) {
        return new RangeOfDays(from, to);
    }

    @Override
    public Iterator<LocalDate> iterator() {
        return new DaysIterator(from);
    }

    private class DaysIterator implements Iterator<LocalDate> {
        private LocalDate cursor;

        private DaysIterator(LocalDate from) {
            cursor = from;
        }

        @Override
        public boolean hasNext() {
            return !cursor.isAfter(to);
        }

        @Override
        public LocalDate next() {
            LocalDate next = LocalDate.from(cursor);
            cursor = cursor.plusDays(1);
            return next;
        }
    }
}
