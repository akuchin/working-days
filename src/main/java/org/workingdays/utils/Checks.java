package org.workingdays.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Checks {

    public static void checkFromIsBeforeTo(LocalDate from, LocalDate to) {
        if (from.isAfter(to))
            throw new IllegalArgumentException("From should be before To");
    }

    public static void checkFromIsBeforeTo(LocalDateTime from, LocalDateTime to) {
        if (from.isAfter(to))
            throw new IllegalArgumentException("From should be before To");
    }

}
