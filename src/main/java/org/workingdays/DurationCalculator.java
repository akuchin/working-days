package org.workingdays;

import org.workingdays.strategies.WorkingDaysStrategy;
import org.workingdays.strategies.WorkingHoursStrategy;
import org.workingdays.strategies.days.AllDaysWorkingStrategy;
import org.workingdays.strategies.hours.TwentyFourHoursStrategy;
import org.workingdays.utils.RangeOfDays;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Spliterator;

import static java.util.stream.StreamSupport.stream;
import static org.workingdays.utils.Checks.checkFromIsBeforeTo;

public class DurationCalculator {

    private final WorkingDaysStrategy daysStrategy;
    private final WorkingHoursStrategy hoursStrategy;

    public DurationCalculator() {
        this(new AllDaysWorkingStrategy(), new TwentyFourHoursStrategy());
    }

    public DurationCalculator(WorkingHoursStrategy hoursStrategy) {
        this(new AllDaysWorkingStrategy(), hoursStrategy);
    }

    public DurationCalculator(WorkingDaysStrategy daysStrategy) {
        this(daysStrategy, new TwentyFourHoursStrategy());
    }

    public DurationCalculator(WorkingDaysStrategy daysStrategy, WorkingHoursStrategy hoursStrategy) {
        this.daysStrategy = daysStrategy;
        this.hoursStrategy = hoursStrategy;
    }

    public Duration workingDuration(LocalDateTime from, LocalDateTime to) {
        checkFromIsBeforeTo(from, to);

        LocalDateTime start = firstWorking(from);
        LocalDateTime end = lastWorking(to);
        if (start.isAfter(end))
            return Duration.ZERO;

        Duration duration = firstPlusLastDaysDuration(start, end);

        long days = exclusiveWorkingDays(start, end);
        return duration.plus(hoursStrategy.dayDuration().multipliedBy(days));
    }

    public long workingDays(LocalDate from, LocalDate to) {
        checkFromIsBeforeTo(from, to);

        Spliterator<LocalDate> days = RangeOfDays.of(from, to).spliterator();
        return stream(days, false)
                .filter(daysStrategy::isWorkingDay)
                .count();
    }

    private long exclusiveWorkingDays(LocalDateTime start, LocalDateTime end) {
        return workingDays(start.toLocalDate(), end.toLocalDate()) - 2;
    }

    private Duration firstPlusLastDaysDuration(LocalDateTime first, LocalDateTime last) {
        Duration toDayEnd = hoursStrategy.toEndOfDay(first);
        Duration fromDayBegin = hoursStrategy.fromStartOfDay(last);
        return toDayEnd.plus(fromDayBegin);
    }

    private LocalDateTime firstWorking(LocalDateTime dateTime) {
        if (!daysStrategy.isWorkingDay(dateTime.toLocalDate()))
            return firstWorking(dateTime.toLocalDate().atStartOfDay().plusDays(1));

        return dateTime;
    }

    private LocalDateTime lastWorking(LocalDateTime dateTime) {
        if (!daysStrategy.isWorkingDay(dateTime.toLocalDate()))
            return lastWorking(dateTime.toLocalDate().atStartOfDay().minusNanos(1));

        return dateTime;
    }

}
