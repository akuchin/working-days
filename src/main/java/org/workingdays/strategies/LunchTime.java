package org.workingdays.strategies;

import java.time.Duration;
import java.time.LocalTime;

public interface LunchTime {

    Duration duration();

    LocalTime begin();

    LocalTime end();

    boolean isLunchTime(LocalTime time);

    boolean isAfterLunch(LocalTime time);

    boolean isBeforeLunch(LocalTime time);

}
