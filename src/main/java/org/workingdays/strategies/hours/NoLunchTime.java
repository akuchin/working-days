package org.workingdays.strategies.hours;

import org.workingdays.strategies.LunchTime;

import java.time.Duration;
import java.time.LocalTime;

public class NoLunchTime implements LunchTime {

    @Override
    public Duration duration() {
        return Duration.ZERO;
    }

    @Override
    public LocalTime begin() {
        return null;
    }

    @Override
    public LocalTime end() {
        return null;
    }

    @Override
    public boolean isLunchTime(LocalTime time) {
        return false;
    }

    @Override
    public boolean isAfterLunch(LocalTime time) {
        return false;
    }

    @Override
    public boolean isBeforeLunch(LocalTime time) {
        return false;
    }
}
