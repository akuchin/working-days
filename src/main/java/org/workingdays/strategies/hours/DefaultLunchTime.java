package org.workingdays.strategies.hours;

import org.workingdays.strategies.LunchTime;

import java.time.Duration;
import java.time.LocalTime;

public class DefaultLunchTime implements LunchTime {

    private final LocalTime begin;
    private final LocalTime end;

    public static LunchTime lunchBetween(int begin, int end) {
        return new DefaultLunchTime(LocalTime.of(begin, 0), LocalTime.of(end, 0));
    }

    public DefaultLunchTime(LocalTime begin, LocalTime end) {
        this.begin = begin;
        this.end = end;
    }

    public Duration duration() {
        return Duration.between(begin, end);
    }

    @Override
    public LocalTime begin() {
        return begin;
    }

    @Override
    public LocalTime end() {
        return end;
    }

    public boolean isLunchTime(LocalTime time) {
        return time.isAfter(begin) && time.isBefore(end);
    }

    public boolean isAfterLunch(LocalTime time) {
        return time.isAfter(end);
    }

    public boolean isBeforeLunch(LocalTime time) {
        return time.isBefore(begin);
    }

}
