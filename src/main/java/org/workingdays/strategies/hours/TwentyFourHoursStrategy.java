package org.workingdays.strategies.hours;

import org.workingdays.strategies.WorkingHoursStrategy;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TwentyFourHoursStrategy implements WorkingHoursStrategy {

    private static final Duration TWENTY_FOUR_HOURS = Duration.of(24, ChronoUnit.HOURS);

    @Override
    public Duration dayDuration() {
        return TWENTY_FOUR_HOURS;
    }

    @Override
    public Duration fromStartOfDay(LocalDateTime dateTime) {
        return Duration.between(dateTime.toLocalDate().atStartOfDay(), dateTime);
    }

    @Override
    public Duration toEndOfDay(LocalDateTime dateTime) {
        return Duration.between(dateTime, dateTime.toLocalDate().atStartOfDay().plusDays(1));
    }
}
