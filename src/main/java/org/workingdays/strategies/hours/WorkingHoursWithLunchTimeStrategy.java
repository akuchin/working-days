package org.workingdays.strategies.hours;

import org.workingdays.strategies.LunchTime;
import org.workingdays.strategies.WorkingHoursStrategy;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.time.Duration.ZERO;

public class WorkingHoursWithLunchTimeStrategy implements WorkingHoursStrategy {

    private final LocalTime dayBegin;
    private final LocalTime dayEnd;
    private final LunchTime lunchTime;

    public WorkingHoursWithLunchTimeStrategy() {
        this(LocalTime.of(9, 0), LocalTime.of(18, 0), new NoLunchTime());
    }

    public WorkingHoursWithLunchTimeStrategy(LunchTime lunchTime) {
        this(LocalTime.of(9, 0), LocalTime.of(18, 0), lunchTime);
    }

    public WorkingHoursWithLunchTimeStrategy(LocalTime dayBegin, LocalTime dayEnd, LunchTime lunchTime) {
        this.dayBegin = dayBegin;
        this.dayEnd = dayEnd;
        this.lunchTime = lunchTime;
    }

    @Override
    public Duration dayDuration() {
        return Duration.between(dayBegin, dayEnd).minus(lunchTime.duration());
    }

    @Override
    public Duration fromStartOfDay(LocalDateTime dateTime) {
        Duration rawDuration = Duration.between(dayBegin, dateTime);
        LocalTime time = dateTime.toLocalTime();
        if (time.isBefore(dayBegin)) return ZERO;
        if (time.isAfter(dayEnd)) return dayDuration();
        if (lunchTime.isLunchTime(time)) return beforeLunch();
        if (lunchTime.isAfterLunch(time)) return rawDuration.minus(lunchTime.duration());
        return rawDuration;
    }

    @Override
    public Duration toEndOfDay(LocalDateTime dateTime) {
        Duration rawDuration = Duration.between(dayEnd, dateTime).negated();
        LocalTime time = dateTime.toLocalTime();
        if (time.isBefore(dayBegin)) return dayDuration();
        if (time.isAfter(dayEnd)) return ZERO;
        if (lunchTime.isLunchTime(time)) return afterLunch();
        if (lunchTime.isBeforeLunch(time)) return rawDuration.minus(lunchTime.duration());
        return rawDuration;
    }


    private Duration afterLunch() {
        return Duration.between(lunchTime.end(), dayEnd);
    }

    private Duration beforeLunch() {
        return Duration.between(dayBegin, lunchTime.begin());
    }

}
