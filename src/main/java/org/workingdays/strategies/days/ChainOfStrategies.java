package org.workingdays.strategies.days;

import org.workingdays.strategies.WorkingDaysStrategy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ChainOfStrategies implements WorkingDaysStrategy {

    private List<WorkingDaysStrategy> strategies = new ArrayList<>();

    @Override
    public boolean isWorkingDay(LocalDate date) {
        for (WorkingDaysStrategy strategy : strategies) {
            if (!strategy.isWorkingDay(date))
                return false;
        }
        return true;
    }

    private ChainOfStrategies(List<WorkingDaysStrategy> strategies, WorkingDaysStrategy strategy) {
        strategies.add(strategy);
        this.strategies = strategies;
    }

    public static ChainOfStrategies of(WorkingDaysStrategy strategy) {
        return new ChainOfStrategies(new ArrayList<>(), strategy);
    }

    public ChainOfStrategies with(WorkingDaysStrategy strategy) {
        return new ChainOfStrategies(strategies, strategy);
    }

}
