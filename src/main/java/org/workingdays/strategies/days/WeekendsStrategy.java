package org.workingdays.strategies.days;

import org.workingdays.strategies.WorkingDaysStrategy;

import java.time.LocalDate;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.ChronoField.DAY_OF_WEEK;

public class WeekendsStrategy implements WorkingDaysStrategy {

    @Override
    public boolean isWorkingDay(LocalDate date) {
        int dayOfWeek = date.get(DAY_OF_WEEK);
        return dayOfWeek != SATURDAY.getValue() && dayOfWeek != SUNDAY.getValue();
    }
}
