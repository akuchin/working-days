package org.workingdays.strategies.days;

import org.workingdays.strategies.WorkingDaysStrategy;

import java.time.LocalDate;

public class AllDaysWorkingStrategy implements WorkingDaysStrategy {

    @Override
    public boolean isWorkingDay(LocalDate date) {
        return true;
    }
}
