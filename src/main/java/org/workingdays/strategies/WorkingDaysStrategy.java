package org.workingdays.strategies;

import java.time.LocalDate;

public interface WorkingDaysStrategy {

    boolean isWorkingDay(LocalDate date);

}
