package org.workingdays.strategies;

import java.time.Duration;
import java.time.LocalDateTime;

public interface WorkingHoursStrategy {

    Duration dayDuration();

    Duration fromStartOfDay(LocalDateTime dateTime);

    Duration toEndOfDay(LocalDateTime dateTime);

}
