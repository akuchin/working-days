package org.workingdays.strategies.hours;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static java.time.Duration.ZERO;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.workingdays.strategies.hours.DefaultLunchTime.lunchBetween;

public class WorkingHoursWithLunchTimeStrategyTest {

    private WorkingHoursWithLunchTimeStrategy testedInstance = new WorkingHoursWithLunchTimeStrategy(lunchBetween(13, 14));

    @Test
    public void testDurationForWorkingHoursBeforeLunch() {
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(10, 30);

        Duration oneHour30Min = Duration.of(1, HOURS).plusMinutes(30);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(oneHour30Min);

        Duration sixHours30Min = Duration.of(6, HOURS).plusMinutes(30);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(sixHours30Min);
    }

    @Test
    public void testDurationForWorkingHoursAfterLunch() {
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(16, 30);

        Duration sixHours30Min = Duration.of(6, HOURS).plusMinutes(30);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(sixHours30Min);

        Duration oneHour30Min = Duration.of(1, HOURS).plusMinutes(30);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(oneHour30Min);
    }

    @Test
    public void testDurationForLunchTime() {
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(13, 30);

        Duration fourHours = Duration.of(4, HOURS);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(fourHours);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(fourHours);
    }

    @Test
    public void testDurationAfterDayEnd() {
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(18, 30);

        Duration eightHours = Duration.of(8, HOURS);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(eightHours);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(ZERO);
    }

    @Test
    public void testDurationBeforeDayBegin() {
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(8, 30);

        Duration eightHours = Duration.of(8, HOURS);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(ZERO);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(eightHours);
    }

    @Test
    public void testDurationBeforeDayBeginWithoutLunch() {
        testedInstance = new WorkingHoursWithLunchTimeStrategy();
        LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(8, 30);

        Duration nineHours = Duration.of(9, HOURS);
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(ZERO);
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(nineHours);
    }

}