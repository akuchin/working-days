package org.workingdays.strategies.hours;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static java.time.temporal.ChronoUnit.HOURS;
import static org.assertj.core.api.Assertions.assertThat;

public class TwentyFourHoursStrategyTest {

    private TwentyFourHoursStrategy testedInstance = new TwentyFourHoursStrategy();

    private LocalDateTime dateTime = LocalDate.of(2016, Month.MAY, 6).atTime(10, 0);

    @Test
    public void testFromStartOfDay() {
        assertThat(testedInstance.fromStartOfDay(dateTime)).isEqualTo(Duration.of(10, HOURS));
    }

    @Test
    public void testToEndOfDay() {
        assertThat(testedInstance.toEndOfDay(dateTime)).isEqualTo(Duration.of(14, HOURS));
    }

}