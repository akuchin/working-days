package org.workingdays.strategies.days;

import org.junit.Before;
import org.junit.Test;
import org.workingdays.strategies.WorkingDaysStrategy;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

public class ChainOfStrategiesTest {

    private static final LocalDate FIRST_DAY_OFF = LocalDate.of(2016, Month.MAY, 11);
    private static final LocalDate SECOND_DAY_OFF = LocalDate.of(2016, Month.MAY, 12);
    private static final LocalDate WORKING_DAY = LocalDate.of(2016, Month.MAY, 13);


    private ChainOfStrategies testedInstance;

    private WorkingDaysStrategy strategyFirst = day -> !FIRST_DAY_OFF.equals(day);
    private WorkingDaysStrategy strategySecond = day -> !SECOND_DAY_OFF.equals(day);

    @Before
    public void setUp() throws Exception {
        testedInstance = ChainOfStrategies
                .of(strategyFirst)
                .with(strategySecond);
    }

    @Test
    public void shouldFailOnFirstStrategy() {
        assertThat(testedInstance.isWorkingDay(FIRST_DAY_OFF)).isFalse();
    }

    @Test
    public void shouldPassFirstAndFailsOnSecondStrategy() {
        assertThat(testedInstance.isWorkingDay(SECOND_DAY_OFF)).isFalse();
    }

    @Test
    public void dayIsWorking() {
        assertThat(testedInstance.isWorkingDay(WORKING_DAY)).isTrue();
    }

}