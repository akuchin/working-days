package org.workingdays.strategies.days;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

public class WeekendsStrategyTest {

    private WeekendsStrategy testedInstance = new WeekendsStrategy();

    @Test
    public void daysFromMondayToFridayAreWorking() {
        LocalDate monday = LocalDate.of(2016, Month.MAY, 2);
        LocalDate tuesday = monday.plusDays(1);
        LocalDate wednesday = monday.plusDays(2);
        LocalDate thursday = monday.plusDays(3);
        LocalDate friday = monday.plusDays(4);

        assertThat(testedInstance.isWorkingDay(monday)).isTrue();
        assertThat(testedInstance.isWorkingDay(tuesday)).isTrue();
        assertThat(testedInstance.isWorkingDay(wednesday)).isTrue();
        assertThat(testedInstance.isWorkingDay(thursday)).isTrue();
        assertThat(testedInstance.isWorkingDay(friday)).isTrue();
    }

    @Test
    public void saturdayAndSundayAreNonWorking() {
        LocalDate saturday = LocalDate.of(2016, Month.MAY, 7);
        LocalDate sunday = LocalDate.of(2016, Month.MAY, 8);
        assertThat(testedInstance.isWorkingDay(saturday)).isFalse();
        assertThat(testedInstance.isWorkingDay(sunday)).isFalse();
    }

}