package org.workingdays;

import org.junit.Before;
import org.junit.Test;
import org.workingdays.strategies.WorkingDaysStrategy;
import org.workingdays.strategies.hours.WorkingHoursWithLunchTimeStrategy;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static java.time.temporal.ChronoUnit.HOURS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.workingdays.strategies.hours.DefaultLunchTime.lunchBetween;

public class DurationCalculatorTest {

    private static final LocalDate DAY_OFF = LocalDate.of(2016, Month.MAY, 5);


    private DurationCalculator testedInstance;

    @Before
    public void setUp() throws Exception {
        WorkingDaysStrategy workingDaysStrategy = day -> !DAY_OFF.equals(day);

        testedInstance = new DurationCalculator(
                workingDaysStrategy,
                new WorkingHoursWithLunchTimeStrategy(lunchBetween(13, 14)));
    }

    @Test
    public void testSameWorkingDayReturnsOne() {
        long result = testedInstance.workingDays(
                LocalDate.of(2016, Month.MAY, 6),
                LocalDate.of(2016, Month.MAY, 6));

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void testWorkingDays() {
        long result = testedInstance.workingDays(
                LocalDate.of(2016, Month.MAY, 4),
                LocalDate.of(2016, Month.MAY, 6));

        assertThat(result).isEqualTo(2);
    }

    @Test
    public void testDuration() {
        LocalDateTime from = LocalDate.of(2016, Month.MAY, 4).atTime(10, 0);
        LocalDateTime to = LocalDate.of(2016, Month.MAY, 6).atTime(11, 0);

        assertThat(testedInstance.workingDuration(from, to)).isEqualTo(Duration.of(7 + 2, HOURS));
    }

    @Test
    public void testDurationSameDay() {
        LocalDateTime from = LocalDate.of(2016, Month.MAY, 6).atTime(10, 0);
        LocalDateTime to = LocalDate.of(2016, Month.MAY, 6).atTime(11, 0);

        assertThat(testedInstance.workingDuration(from, to)).isEqualTo(Duration.of(1, HOURS));
    }

    @Test
    public void testDurationFirstDayIsDayOf() {
        LocalDateTime from = DAY_OFF.atTime(10, 0);
        LocalDateTime to = LocalDate.of(2016, Month.MAY, 7).atTime(11, 0);

        assertThat(testedInstance.workingDuration(from, to)).isEqualTo(Duration.of(10, HOURS));
    }

    @Test
    public void testDurationLastDayIsDayOf() {
        LocalDateTime from = LocalDate.of(2016, Month.MAY, 3).atTime(17, 0);
        LocalDateTime to = DAY_OFF.atTime(10, 0);

        assertThat(testedInstance.workingDuration(from, to)).isEqualTo(Duration.of(9, HOURS));
    }

    @Test
    public void wholeDayDefaultStrategy() {
        testedInstance = new DurationCalculator();

        LocalDateTime from = LocalDate.of(2016, Month.MAY, 4).atTime(12, 0);
        LocalDateTime to = LocalDate.of(2016, Month.MAY, 6).atTime(11, 0);

        assertThat(testedInstance.workingDuration(from, to)).isEqualTo(Duration.of(12 + 24 + 11, HOURS));
    }

}